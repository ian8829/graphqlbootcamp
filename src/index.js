import { GraphQLServer } from 'graphql-yoga';

// Scalar Types - String, Boolean, Int, Float, ID

// Demo user data
const users = [{
  id: '1',
  name: 'Ian',
  email: 'ian@gmail.com',
  age: 30
}, {
  id: '2',
  name: 'Sarah',
  email: 'sarah@gmail.com',
  age: 20
}, {
  id: '3',
  name: 'Mike',
  email: 'mike@gmail.com',
  age: 29
}];

const posts = [{
  id: '10',
  title: 'GraphQL 10',
  body: 'This is basic course',
  published: true,
  author: '1'
}, {
  id: '11',
  title: 'GraphQL 11',
  body: 'This is advanced course',
  published: false,
  author: '1'
}, {
  id: '12',
  title: 'GraphQL 12',
  body: 'This is skillful course',
  published: true,
  author: '2'
}];

// Type definitions (schema)
// exclamation mark means not null
const typeDefs = `
  type Query {
    me: User!
    post: Post!
    users(query: String): [User!]!
    posts(query: String): [Post!]!
  }

  type User {
    id: ID!
    name: String!
    email: String!
    age: Int
    posts: [Post!]!
  }

  type Post {
    id: ID!
    title: String!
    body: String!
    published: Boolean!
    author: User!
  }
`;

// Resolvers
const resolvers = {
  Query: {
    me() {
      return {
        id: '12398',
        name: 'Mike',
        email: 'mike@example.com'
      }
    },
    post() {
      return {
        id: '092',
        title: 'Marchal',
        published: true
      }
    },
    users(parent, args, ctx, info) {
      if (!args.query) {
        return users
      }
      return users.filter((user) => {
        return user.name.toLocaleLowerCase().includes(args.query.toLocaleLowerCase())
      })
    },
    posts(parent, args, ctx, info) {
      if (!args.query) {
        return posts
      }
      return posts.filter((post) => {
        const isTitleMatch = post.title.toLocaleLowerCase().includes(args.query.toLocaleLowerCase())
        const isBodyMatch = post.body.toLocaleLowerCase().includes(args.query.toLocaleLowerCase())
        return isTitleMatch || isBodyMatch
      })
    }
  },
  Post: {
    author(parent, args, ctx, info) {
      return users.find((user) => {
        return user.id === parent.author
      })
    }
  },
  User: {
    posts(parent, args, ctx, info) {
      return posts.filter((post) => {
        return post.author === parent.id
      })
    }
  }
}

// query {
//   users {
//     id
//     name
//     email
//     age
//     posts {
//       id
//       title
//       author {
//         id
//         name
//       }
//     }
//   }
// }

const server = new GraphQLServer({
  typeDefs,
  resolvers
});

server.start((opt) => {
  // yoga use localhost:4000
  console.log('The server is up at port', opt.port);
});